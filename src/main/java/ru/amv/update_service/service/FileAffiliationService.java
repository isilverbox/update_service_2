package ru.amv.update_service.service;

import org.springframework.stereotype.Component;
import org.thymeleaf.util.ListUtils;
import ru.amv.update_service.dto.FileInfo;
import ru.amv.update_service.enums.OperationType;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */

@Component
public class FileAffiliationService {
//	public static void main(String[] args) {
//		List<String> aList = new ArrayList<>(Arrays.asList("1", "2", "4"));
//		List<String> bList = new ArrayList<>(Arrays.asList("1", "2", "3"));
//
//		final Set<String> temp = new HashSet<>(aList);
//		aList.removeAll(bList);
//		bList.removeAll(temp);
//
//		aList.addAll(bList);
//		System.out.println("result");
//		aList.forEach(s -> System.out.print(s));// 4,3
//	}
	private FileInfoGrabber fileInfoGrabber;

	public FileAffiliationService(FileInfoGrabber fileInfoGrabber) {
		this.fileInfoGrabber = fileInfoGrabber;
	}

	public List<FileInfo> getProcessedFileInfoList(List<FileInfo> clientFileInfoList) {
		List<FileInfo> serverFileInfoList = fileInfoGrabber.getServerFileInfoList();

		clientFileInfoList.forEach(fileInfo -> fileInfo.setOperationType(OperationType.DELETE));

		final Set<FileInfo> serverTemp = new HashSet<>(serverFileInfoList);
		serverFileInfoList.removeAll(clientFileInfoList);
		clientFileInfoList.removeAll(serverTemp);
		serverFileInfoList.addAll(clientFileInfoList);

		return serverFileInfoList;
	}
}
