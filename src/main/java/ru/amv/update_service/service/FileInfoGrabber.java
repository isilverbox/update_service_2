package ru.amv.update_service.service;

import org.springframework.stereotype.Component;
import ru.amv.update_service.dto.FileInfo;
import ru.amv.update_service.configuration.FilesPath;
import ru.amv.update_service.configuration.FilesUrl;
import ru.amv.update_service.enums.FileType;
import ru.amv.update_service.enums.OperationType;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Server files grabber
 */
@Component
public class FileInfoGrabber {
	private FilesPath filesPath;
	private FilesUrl filesUrl;

	public FileInfoGrabber(FilesUrl filesUrl, FilesPath filesPath) {
		this.filesUrl = filesUrl;
		this.filesPath = filesPath;
	}

	public List<FileInfo> getServerFileInfoList() {
		List<FileInfo> fileInfoList = new ArrayList<>();

		List<FileInfo> appList = getServerFileInfoList(filesPath.getApp(), FileType.APP);
		List<FileInfo> libList = getServerFileInfoList(filesPath.getLib(), FileType.LIB);

		if (appList != null) fileInfoList.addAll(appList);
		if (libList != null) fileInfoList.addAll(libList);

		return fileInfoList;
	}

	private List<FileInfo> getServerFileInfoList(String path, FileType fileType) {
		try {
			return Stream.of(Objects.requireNonNull(new File(path).listFiles())).
					filter(file -> !file.isDirectory()).
					map(file -> new FileInfo(file.getName(), file.length(), getFileMd5(file), filesUrl.buildUrl(file), fileType, OperationType.ADD)).
					collect(Collectors.toList());
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
		return null;
	}

	private String getFileMd5(File file) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");

			//Get file input stream for reading the file content
			FileInputStream fis = new FileInputStream(file);

			//Create byte array to read data in chunks
			byte[] byteArray = new byte[1024];
			int bytesCount = 0;

			//Read file data and update in message digest
			while ((bytesCount = fis.read(byteArray)) != -1)
				messageDigest.update(byteArray, 0, bytesCount);

			fis.close();

			//Get the hash's bytes
			byte[] bytes = messageDigest.digest();

			//This bytes[] has bytes in decimal format;
			//Convert it to hexadecimal format
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				stringBuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			//return complete hash
			return stringBuilder.toString();
		} catch (NoSuchAlgorithmException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


}
