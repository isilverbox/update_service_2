package ru.amv.update_service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.amv.update_service.enums.FileType;
import ru.amv.update_service.enums.OperationType;

import java.util.Objects;

/**
 * Объект хранящий всю информацию о файле
 * используется как на стороне сервера так и на стороне клиента
 */

public class FileInfo {

	@JsonProperty("name")
	private String name;

	@JsonProperty("size")
	private long size;

	@JsonProperty("md5")
	private String md5;

	@JsonProperty("url")
	private String url;

	@JsonProperty("fileType")
	private FileType fileType;

	@JsonProperty("operationType")
	private OperationType operationType;

	public FileInfo(String name, long size, String md5, String url, FileType fileType, OperationType operationType) {
		this.name = name;
		this.size = size;
		this.md5 = md5;
		this.url = url;
		this.fileType = fileType;
		this.operationType = operationType;
	}

	public FileInfo() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, size, md5, fileType);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (!(obj instanceof FileInfo)) return false;

		FileInfo fileInfo = (FileInfo) obj;

		return Objects.equals(name, fileInfo.getName()) &&
				size == fileInfo.getSize() &&
				Objects.equals(md5, fileInfo.getMd5()) &&
				Objects.equals(fileType, fileInfo.getFileType());
	}

	@Override
	public String toString() {
		return "FileInfo{" +
				"name='" + name + '\'' +
				", size=" + size +
				", md5='" + md5 + '\'' +
				", url='" + url + '\'' +
				", fileType=" + fileType +
				", operationType=" + operationType +
				'}';
	}
}
