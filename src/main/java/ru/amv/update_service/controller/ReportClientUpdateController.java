package ru.amv.update_service.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.amv.update_service.configuration.FilesPath;
import ru.amv.update_service.dto.FileInfo;
import ru.amv.update_service.service.FileAffiliationService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

//@Controller
@RequestMapping("/report_client")
//@RestController combines @Controller and @ResponseBody, two annotations that results in web requests
// returning data rather than a view.
@RestController
public class ReportClientUpdateController {
	private final FilesPath filesPath;
	private final FileAffiliationService fileAffiliationService;

	public ReportClientUpdateController(FilesPath filesPath,
										FileAffiliationService fileAffiliationService) {
		this.filesPath = filesPath;
		this.fileAffiliationService = fileAffiliationService;
	}

	@GetMapping
	public String index() {
		return "hello";
	}

	@PostMapping("/client_files")
	public String client_files(@RequestBody @Valid List<FileInfo> fileInfoList,
							   BindingResult bindingResult) {

		if (bindingResult.hasErrors()) return null;

		List<FileInfo> processedList = fileAffiliationService.getProcessedFileInfoList(fileInfoList);

		String result = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			result = objectMapper.writeValueAsString(processedList);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@GetMapping("/files/**")
	public ResponseEntity<FileSystemResource> downloadStuff(HttpServletRequest request) {

		String uri = request.getRequestURI().substring(15);// /report_client/ length

		File file = new File(filesPath.getParent() + uri);
		long fileLength = file.length();
		System.out.println("file path: " + file.getAbsolutePath() + ", size: " + fileLength);

		if (fileLength == 0)
			throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		respHeaders.setContentLength(fileLength);
		respHeaders.setContentDispositionFormData("attachment", file.getName());

		return new ResponseEntity<FileSystemResource>(
				new FileSystemResource(file), respHeaders, HttpStatus.OK);
	}
}
