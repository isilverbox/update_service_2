package ru.amv.update_service.enums;

/**
 * File operation type
 */
public enum OperationType {
	ADD, UPDATE, DELETE
}
