package ru.amv.update_service.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class FilesUrl {
	private String utl;

	public FilesUrl(@Value("${address.url}") String utl) {
		this.utl = utl;
	}

	public String getUtl() {
		return utl;
	}

	public void setUtl(String utl) {
		this.utl = utl;
	}

	public String buildUrl(File file) {
		return utl + "/" + file.toPath().toString().replaceAll("\\\\", "/");
	}
}
