package ru.amv.update_service.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Latest files sources
 */
@Configuration
public class FilesPath {
	private String parent;
	private String app;
	private String lib;

	public FilesPath(@Value("${path.files.parent}") String parent,
					 @Value("${path.files.app.report_client.stable}") String app,
					 @Value("${path.files.app.report_client.stable.lib}") String lib) {
		this.parent = parent;
		this.app = app;
		this.lib = lib;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		this.lib = lib;
	}
}
